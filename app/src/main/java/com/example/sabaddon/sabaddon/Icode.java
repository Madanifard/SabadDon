package com.example.sabaddon.sabaddon;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Hello_world on 10/17/2017.
 */

public class Icode  {

    /**
     * for show the toast
     * @param mContext
     * @param message
     */
    public static void ShowShortToast(Context mContext, String message) {

        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }
}
