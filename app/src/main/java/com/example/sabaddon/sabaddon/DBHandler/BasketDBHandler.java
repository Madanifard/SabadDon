package com.example.sabaddon.sabaddon.DBHandler;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQuery;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by Hello_world on 11/7/2017.
 */

public class BasketDBHandler extends SQLiteOpenHelper {

    String createBasketTable = "CREATE TABLE baskets (" +
            "id INTEGER AUTO INCREMENT PRIMARY KEY ," +
            "server_id INTEGER ," +
            "name TEXT ," +
            "active INTEGER" + ")";

    String createBasketItemId = "CREATE TABLE basket_items (" +
            "id INTEGER AUTO INCREMENT PRIMARY KEY , " +
            "local_basket_id INTEGER ," +
            "server_basket_id INTEGER , " +
            "unit_server_id INTEGER , " +
            "product_id INTEGER , " +
            "count INTEGER , " +
            "date_buy TEXT , " +
            "price INTEGER" + ")";

    String createProducts = "CREATE TABLE products (" +
            "id INTEGER AUTO INCREMENT PRIMARY KEY ," +
            "server_id INTEGER ," +
            "name STRING" + ")";

    String createUnits = "CREATE TABLE units (" +
            "id INTEGER AUTO INCREMENT PRIMARY KEY ," +
            "server_id INTEGER, " +
            "name STRING" + ")";

    public BasketDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(createBasketTable);
        db.execSQL(createBasketItemId);
        db.execSQL(createProducts);
        db.execSQL(createUnits);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }



    /**
     * for insert BasketItem ID
     * @param localId
     * @param serverId
     * @param productId
     * @param count
     * @return
     */
    public int insertBasketItem(int localId, int serverId, int productId, int count, int unitServerId) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("local_basket_id", localId);
        contentValues.put("server_basket_id", serverId);
        contentValues.put("product_id", productId);
        contentValues.put("count", count);
        contentValues.put("unit_server_id", unitServerId);

        return  (int) db.insert("basket_items", null, contentValues);

    }

    /**
     * for Insert the Basket and return the Id
     * @param name
     * @return
     */
    public int insertBasket(int server_id,String name) {

        try {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("name", name);
            values.put("server_id", server_id);
            values.put("active", 1);
            return (int)db.insert("baskets", null, values);

        } catch (Exception ex) {

            return -1;
        }

    }

    /**
     * find active Basket and return id => name
     * @return
     */
    public String[][] selectActiveBasket() {

        String basketsQuery = "SELECT baskets.server_id, baskets.name FROM baskets WHERE active = 1" ;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(basketsQuery, null);

        String basketsInfo[][] = new String[cursor.getCount()][3];

        int i = 0;
        while(cursor.moveToNext()) {

            basketsInfo[i][0] = cursor.getString(1);
            basketsInfo[i][1] = getCountBasketItem(Integer.parseInt(cursor.getString(0)));
            basketsInfo[i][2] = cursor.getString(0);
            i++;
        }

        return basketsInfo;
    }

    /**
     * get count basket Item in baskets
     * @param serverBasketId
     * @return
     */
    public String getCountBasketItem(int serverBasketId) {

        String countQuery = "SELECT count(*) FROM basket_items WHERE server_basket_id = " + serverBasketId;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        String count = "0";
        while(cursor.moveToNext()) {
            count = cursor.getString(0);
        }
        return count;

    }

    /**
     * for get the name of basket
     * @param serverBasketId
     * @return
     */
    public String getNameBasket(int serverBasketId) {

        String query = "SELECT name FROM baskets WHERE server_id = " + serverBasketId;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String name = "";
        while (cursor.moveToNext()) {
            name = cursor.getString(0);
        }

        return name;
    }

    /**
     * get Active Basket Item
     * @param serverBasketId
     * @return
     */
    public String[][] getActiveBasketItem(int serverBasketId) {

        String query = "SELECT server_basket_id, product_id, count,  date_buy, price, unit_server_id " +
                "FROM basket_items WHERE server_basket_id = " + serverBasketId;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String itemList[][] = new String[cursor.getCount()][8];
        int i = 0;
        while (cursor.moveToNext()) {

            itemList[i][0] = cursor.getString(0); //server_basket_id
            itemList[i][1] = getNameProduct(Integer.parseInt(cursor.getString(1))); //product_id => nameProduct
            itemList[i][2] = cursor.getString(2); //count
            //date_buy
            if(cursor.getString(3) == null) {
                itemList[i][3] = "NotBuy";
            } else {
                itemList[i][3] = cursor.getString(3);
            }
            //price
            if(cursor.getString(4) == null) {
                itemList[i][4] = "NotPrice";
            } else {
                itemList[i][4] = cursor.getString(4);
            }

            itemList[i][5] = getNameUnit(Integer.parseInt(cursor.getString(5))); // unit_server_id

            itemList[i][6] = cursor.getString(1); //product_id

            i++;
        }

        return itemList;
    }

    /**
     * insert product if not Exists
     * @param serverProductId
     * @param nameProduct
     */
    public void manageProduct(int serverProductId, String nameProduct) {


        String queryCheck = "SELECT id, server_id FROM products WHERE server_id = " + serverProductId;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(queryCheck, null);

        if(cursor.moveToFirst() == false) {

            ContentValues values = new ContentValues();
            values.put("server_id", serverProductId);
            values.put("name", nameProduct);

            db.insert("products", null, values);

        }
    }

    /**
     * get server_id and return name
     * @param serverProductId
     * @return
     */
    public String getNameProduct(int serverProductId) {

        String query = "SELECT name, server_id FROM products WHERE server_id = " + serverProductId;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String name = "";
        while (cursor.moveToNext()) {
            name = cursor.getString(0);
        }

        return name;
    }

    /**
     * if not Exists then insert
     * @param serveId
     * @param name
     * @return
     */
    public void manageUnits(int serveId, String name) {

        String checkExists = "SELECT server_id, name FROM units WHERE server_id = " + serveId;
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(checkExists, null);

        if(cursor.moveToFirst() == false) {

            ContentValues values = new ContentValues();
            values.put("server_id", serveId);
            values.put("name", name);

            db.insert("units", null, values);

        } else {

            while (cursor.moveToNext()) {
                if(!cursor.getString(1).equals(name)) {
                   //then update data
                    String updateUnit = "UPDATE units SET name = '" + name + "' WHERE server_id = " + serveId;
                    db.rawQuery(updateUnit, null);
                }
            }
        }
    }

    /**
     * for get name unit
     * @param serverId
     * @return
     */
    public String getNameUnit(int serverId) {

        String query = "SELECT name, server_id FROM units WHERE server_id = " + serverId;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String name = "notSet";
        while(cursor.moveToNext()) {
            name = cursor.getString(0);
        }

        return name;
    }

    /**
     * update name Basket
     * @param id
     * @param name
     */
    public void updateNameBasket(int id, String name) {

//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues contentValues = new ContentValues();
//        contentValues.put("name", name);
//
//        String whereClause = "id=?";
//        String whereArgs[] = {Integer.toString(id)};
//
//
//        db.update("products", contentValues, whereClause,  whereArgs);

        String update = "UPDATE baskets SET name = '" + name + "' WHERE server_id = " + id ;

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(update);
        db.close();

    }

    /**
     * update the info Basket
     * @param productItemId
     * @param unitServerId
     * @param count
     * @param date_buy
     * @param price
     */
    public void updateItemBasket(int productItemId, int unitServerId, int count,String date_buy, int price) {

        String updateStatement = "UPDATE basket_items SET unit_server_id = " + unitServerId +
                ", count = " + count +
                ", date_buy = " + "'" + date_buy + "'" +
                ", price = " +  price +
                " WHERE product_id = " + productItemId;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(updateStatement);
        db.close();
    }

    /**
     * get single basketItem info
     * @param productItemId
     * @return
     */
    public String[] getBasketItem(int productItemId) {

        String query = "SELECT product_id, unit_server_id, count, date_buy, price FROM basket_items WHERE product_id = " + productItemId;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String dataItem[] = new String[5];

        while (cursor.moveToNext()) {

            //product_id
            dataItem[0] = getNameProduct(Integer.parseInt(cursor.getString(0))); // get name product

            dataItem[1] = cursor.getString(1);  //unit_id

            dataItem[2] = (cursor.getString(2).equals(null))? "NotSet" : cursor.getString(2);  //count

            dataItem[3] = cursor.getString(3);   //date_buy

            dataItem[4] = cursor.getString(4);  //price
        }

        return dataItem;
    }

    /**
     * for Delete the Item
     * @param server_basket_id
     * @param product_id
     */
    public void deleteBasketItems(int server_basket_id, int product_id) {

        String delete = "DELETE FROM basket_items WHERE product_id = " + product_id + ", server_basket_id = " + server_basket_id;

        SQLiteDatabase db = this.getWritableDatabase();
        db.rawQuery(delete, null);
        db.close();
    }

}
