
package com.example.sabaddon.sabaddon.Models.InsertBasketModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InsertBasketModel {

    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("basket_id")
    @Expose
    private Integer basketId;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getBasketId() {
        return basketId;
    }

    public void setBasketId(Integer basketId) {
        this.basketId = basketId;
    }

}
