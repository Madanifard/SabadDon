
package com.example.sabaddon.sabaddon.Models.InsertBasketItemModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InsertBasketItemModel {

    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("basket_item_id")
    @Expose
    private Integer basketItemId;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getBasketItemId() {
        return basketItemId;
    }

    public void setBasketItemId(Integer basketItemId) {
        this.basketItemId = basketItemId;
    }

}
