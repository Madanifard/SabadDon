package com.example.sabaddon.sabaddon;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.example.sabaddon.sabaddon.Models.TokenModel.TokenModel;
import com.google.gson.Gson;
import com.loopj.android.http.*;

import org.w3c.dom.Text;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Hello_world on 10/15/2017.
 */

public class BaseActivity extends AppCompatActivity {

    Context mContext = this;
    Activity mActivity = this;

    String baseUrl = "http://sabaddon.hotelphp.ir/apis/";

    Long token;

    public void ws_getToken() {

        String urlToken =  baseUrl + "getToken";

        AsyncHttpClient client = new AsyncHttpClient();

        client.get(urlToken, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                Icode.ShowShortToast(mContext, throwable.toString());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                 parseTokenJsonGson(responseString);
            }
        });


    }

    /**
     * parse the json for get token model
     * @param response_string
     * @return
     */
    public void parseTokenJsonGson(String response_string) {

        Gson gson = new Gson();

        TokenModel token = gson.fromJson(response_string, TokenModel.class);

        Log.d("Debug", "debug : getToken : " + token.getToken());
        this.token = Long.parseLong(token.getToken());

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceRTLIfSupported()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

}
