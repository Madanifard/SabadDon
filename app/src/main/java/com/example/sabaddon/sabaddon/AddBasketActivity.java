package com.example.sabaddon.sabaddon;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.sabaddon.sabaddon.Adapters.ProductItemAdapter;
import com.example.sabaddon.sabaddon.DBHandler.BasketDBHandler;
import com.example.sabaddon.sabaddon.Models.GetUnitModel.GetUnitModel;
import com.example.sabaddon.sabaddon.Models.GetUnitModel.Unit;
import com.example.sabaddon.sabaddon.Models.InsertBasketModel.InsertBasketModel;
import com.example.sabaddon.sabaddon.Models.ProductModel.Product;
import com.example.sabaddon.sabaddon.Models.ProductModel.ProductModel;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class AddBasketActivity extends BaseActivity  {

    EditText nameBasket, countProduct;
    Button btnAddBasket;
    Spinner drpProduct, drpUnit;
    LinearLayout layoutAddedProduct;
    ListView lstProductItem;

    ArrayAdapter adapter;

    ArrayList<String> nameProductArray = new ArrayList<String>();
    ArrayList<String> countProductArray = new ArrayList<String>();
    ArrayList<String> unitProductArray = new ArrayList<String>();

    int basketServerId, basketLocalId;
    String basketName;

    BasketDBHandler basketDBHandler ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_basket);

        getProducts();
        getUnits();

        widget();

        basketDBHandler = new BasketDBHandler(mContext, "sabaddon.db", null, 1);
    }

    /**
     * for bind widget
     */
    public void widget() {

        nameBasket = (EditText)findViewById(R.id.name_basket);
        btnAddBasket = (Button)findViewById(R.id.btn_added_basket);

        drpProduct = (Spinner) findViewById(R.id.drp_product);
        drpUnit = (Spinner) findViewById(R.id.drp_unit);
        countProduct = (EditText) findViewById(R.id.count_product);

        layoutAddedProduct =(LinearLayout) findViewById(R.id.layout_added_product);

        lstProductItem = (ListView) findViewById(R.id.lst_product_item);
    }

    /**
     * btn btn_added_basket click
     * @param view
     */
    public void onClickAddedBasket(View view) {

        if(nameBasket.getText().toString().trim().equals("")) {
            Icode.ShowShortToast(mContext, "ابتدا نام سبد خرید را وارد نمایید");
        } else {
//            String token = this.token;
            String token = "Token";
            basketName = nameBasket.getText().toString();
            int memberId = Hawk.get("memberId");

            postInsertBasket(token, basketName, memberId);
        }
    }

    /**
     * call /insert/basket webservice
     */
    public void postInsertBasket(String token, String name, int member_id) {

        String postUrlInsertBasket = baseUrl + "insert/basket";

        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("_token", token);
        params.put("member_id", member_id);
        params.put("name", name);

        client.post(postUrlInsertBasket, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Icode.ShowShortToast(mContext, throwable.toString());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseInsertBasketJsonGson(responseString);
            }

        });
    }

    /**
     * parse /insert/basket webservice
     * @param response_string
     * @return
     */
    public void parseInsertBasketJsonGson(String response_string) {


        Gson gson = new Gson();
        InsertBasketModel basket = gson.fromJson(response_string, InsertBasketModel.class);

        if(basket.getStatusCode().equals(100)) {

            basketServerId = basket.getBasketId();

            basketLocalId = basketDBHandler.insertBasket(basketServerId, basketName); //added to dataBase
            Log.d("Debug", "debug : basketDBHandler.insertBasket : " + basketLocalId);


            layoutAddedProduct.setVisibility(View.VISIBLE);
            btnAddBasket.setVisibility(View.INVISIBLE);

        } else {
            Icode.ShowShortToast(mContext, "Have Error In Send To DataBase");
        }
    }

    /**
     * call wbe service for get Unit
     */
    public void getUnits() {

        String getUnitUrl = baseUrl + "get/units";

        AsyncHttpClient client = new AsyncHttpClient();

        client.get(getUnitUrl, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Icode.ShowShortToast(mContext, throwable.toString());
                Log.d("Debug", " debug :  getProduct : ", throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseUnitGson(responseString);
            }
        });
    }

    /**
     * parse and get Units
     * @param response_string
     */
    public void parseUnitGson(String response_string) {

        Gson gson = new Gson();

        GetUnitModel unitModel = gson.fromJson(response_string, GetUnitModel.class);

        if(unitModel.getStatusCode().equals(200)) {

            ArrayList<String> unitArray = new ArrayList<String>();
            unitArray.add(0, "انتخاب واحد محصول");

            for (Unit unit : unitModel.getUnits()) {
                basketDBHandler.manageUnits(unit.getId(), unit.getName());

                unitArray.add(unit.getId(), unit.getName());
            }

            ArrayAdapter unitAdapter = new ArrayAdapter(this, R.layout.spinner_row ,unitArray);
            drpUnit.setAdapter(unitAdapter);

        } else {
            Icode.ShowShortToast(mContext, "Error in get Units");
        }

    }

    /**
     * call /get/products webservice
     */
    public void getProducts() {

        String getProductUrl = baseUrl + "get/products";

        AsyncHttpClient client = new AsyncHttpClient();

        client.get(getProductUrl, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                Icode.ShowShortToast(mContext, throwable.toString());
                Log.d("Debug", " debug :  getProduct : ", throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                parseProductJsonGson(responseString);

            }
        });
    }

    /**
     *  parse /get/products webservice
     * @param response_string
     */
    public void parseProductJsonGson(String response_string) {

        Gson gson = new Gson();
        ProductModel product = gson.fromJson(response_string, ProductModel.class);

        if(product.getStatusCode().toString().equals("200")) {
            // not Have Error
           ArrayList<String> products = new ArrayList<String>();
            //create Index 0
            products.add(0, "انتخاب از محصولات زیر");
            for (Product item : product.getProducts()) {

                //insert to data base
                basketDBHandler.manageProduct(item.getId(), item.getName());

                products.add(item.getId(), item.getName());

            }
            adapter = new ArrayAdapter(this, R.layout.spinner_row, products);
            drpProduct.setAdapter(adapter);

        } else if (product.getStatusCode().toString().equals("404")) {
            //have Error
            Icode.ShowShortToast(mContext, product.getMessage());
        }
    }

    /**
     * when click the OnClick Button
     * @param view
     */
    public void onClickAddProduct(View view) {

        if(countProduct.getText().toString().equals("")) {
            Icode.ShowShortToast(mContext, "plz Select the Count Product");
        } else {
            postProduct();
        }
    }

    /**
     * post the data to Server
     */
    public void postProduct() {

        nameProductArray.add(drpProduct.getSelectedItem().toString());
        countProductArray.add(countProduct.getText().toString());
        unitProductArray.add(drpUnit.getSelectedItem().toString());

        //insert to basket Item
        int basketItemId = basketDBHandler.insertBasketItem(basketLocalId,
                basketServerId,
                (int) drpProduct.getSelectedItemId(),
                Integer.parseInt(countProduct.getText().toString()),
                (int) drpUnit.getSelectedItemId());

        Log.d("Debug", "debug : basketItemDBHandler : " + basketItemId);

        final String postUrlString = baseUrl + "insert/basket/item";
        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams param = new RequestParams();
        param.put("product_id", drpProduct.getSelectedItemId());
        param.put("count", countProduct.getText().toString());
        param.put("basket_id", basketServerId);


        client.post(postUrlString, param, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                Icode.ShowShortToast(mContext, throwable.toString());

                Log.d("Debug", "debug : insert/basket/item : " + postUrlString, throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseInsertBasketItemGson(responseString);
            }
        });
    }

    /**
     * get Data and Parse the response the WebService and added to ListView
     * @param responseString
     */
    public void parseInsertBasketItemGson(String responseString) {

        Gson gson = new Gson();
        InsertBasketModel item = gson.fromJson(responseString, InsertBasketModel.class);

        if(item.getStatusCode().equals(200)) {

            ProductItemAdapter ProductAdapter = new ProductItemAdapter(mContext,
                                                     nameProductArray.toArray(new String[0]),
                                                     countProductArray.toArray(new String[0]),
                                                     unitProductArray.toArray(new String[0]));
            lstProductItem.setAdapter(ProductAdapter); // for joined adapter

        } else {
          Icode.ShowShortToast(mContext, "Error In Post in database");
        }
    }

}
