package com.example.sabaddon.sabaddon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.sabaddon.sabaddon.Adapters.ActiveBasketAdapter;
import com.example.sabaddon.sabaddon.DBHandler.BasketDBHandler;


public class ActiveBasketActivity extends BaseActivity {

    ListView activeListProduct;

    BasketDBHandler basketDBHandler ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_basket);

        bindWidget();

        basketDBHandler =  new BasketDBHandler(mContext, "sabaddon.db", null, 1);

        String[][] activeBasket = basketDBHandler.selectActiveBasket();
        ActiveBasketAdapter adapter = new ActiveBasketAdapter(mContext, activeBasket);

        activeListProduct.setAdapter(adapter);

        activeListProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                TextView basketServerId = (TextView) view.findViewById(R.id.active_basket_id);
                Icode.ShowShortToast(mContext, basketServerId.getText().toString());

                redirectToSeeDetails(basketServerId.getText().toString());
            }
        });


    }

    /**
     * bind the widget
     */
    public void bindWidget() {

        activeListProduct = (ListView) findViewById(R.id.lst_active_product);
    }

    /**
     * for redirect
     * @param basketServerId
     */
    public void redirectToSeeDetails(String basketServerId) {

        Intent intent = new Intent(mContext, DetailsBasketActivity.class);
        intent.putExtra("serverBasketId", basketServerId);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();

        String[][] activeBasket = basketDBHandler.selectActiveBasket();
        ActiveBasketAdapter adapter = new ActiveBasketAdapter(mContext, activeBasket);

        activeListProduct.setAdapter(adapter);
    }
}
