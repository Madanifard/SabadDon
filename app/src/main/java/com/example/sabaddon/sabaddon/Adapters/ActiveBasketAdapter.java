package com.example.sabaddon.sabaddon.Adapters;

import android.content.Context;
import android.inputmethodservice.Keyboard;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sabaddon.sabaddon.R;

import java.util.ArrayList;

/**
 * Created by Hello_world on 11/11/2017.
 */

public class ActiveBasketAdapter extends BaseAdapter {

    Context mContext;
    String[][] activeBasket;

    public ActiveBasketAdapter(Context mContext, String[][] activeBasket) {

        this.mContext =  mContext;
        this.activeBasket = activeBasket;
    }

    @Override
    public int getCount() {

        return activeBasket.length;
    }

    @Override
    public Object getItem(int position) {

        return activeBasket[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = LayoutInflater.from(mContext).inflate(R.layout.list_active_basket, parent, false);

        TextView nameBasket  = (TextView) rowView.findViewById(R.id.nameBasket);
        TextView countProduct = (TextView) rowView.findViewById(R.id.countProduct);
        TextView basketId = (TextView) rowView.findViewById(R.id.active_basket_id);

        nameBasket.setText(activeBasket[position][0]);
        countProduct.setText(activeBasket[position][1]);
        basketId.setText(activeBasket[position][2]);

        return rowView ;
    }
}
