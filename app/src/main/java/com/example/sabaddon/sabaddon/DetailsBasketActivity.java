package com.example.sabaddon.sabaddon;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.sabaddon.sabaddon.Adapters.InfoBasketAdapter;
import com.example.sabaddon.sabaddon.DBHandler.BasketDBHandler;
import com.example.sabaddon.sabaddon.Models.UpdateBasketModel.UpdateBasketModel;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.zip.Inflater;

import cz.msebera.android.httpclient.Header;

public class DetailsBasketActivity extends BaseActivity {

    int basketServerId;
    TextView activeBasketName ;
    Button nameBasketEditBtn;
    ListView lstItem;

    BasketDBHandler bdHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_basket);

        basketServerId = Integer.parseInt(getIntent().getExtras().get("serverBasketId").toString());

        bdHandler = new BasketDBHandler(mContext, "sabaddon.db", null, 1);

        bindWidget();

        setNameActiveBasket(basketServerId);

        setItems(basketServerId);

        lstItem.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TextView product_item_id = (TextView) view.findViewById(R.id.product_item_id);
                TextView server_basket_id = (TextView) view.findViewById(R.id.server_basket_id);

                Intent intent = new Intent(mContext, EditItemBasketActivity.class);
                intent.putExtra("product_item_id", product_item_id.getText().toString());
                intent.putExtra("server_basket_id", server_basket_id.getText().toString());

                startActivity(intent);
            }

        });

    }

    /**
     * binding
     */
    public void bindWidget() {

        activeBasketName = (TextView) findViewById(R.id.active_basket_name);
        nameBasketEditBtn = (Button) findViewById(R.id.name_basket_edit);

        lstItem = (ListView) findViewById(R.id.item_basket_active);
    }

    /**
     * set name basket active
     * @param serverBasketId
     */
    public void setNameActiveBasket(int serverBasketId) {

        String nameBasket = bdHandler.getNameBasket(serverBasketId);
        activeBasketName.setText(nameBasket);
    }

    /**
     * set Adapter to show Item Basket
     * @param serverBasketId
     */
    public void setItems(int serverBasketId) {

        InfoBasketAdapter adapter = new InfoBasketAdapter(mContext, bdHandler.getActiveBasketItem(serverBasketId));
        lstItem.setAdapter(adapter);
    }

    /**
     * when click the Edit Button Show Dialog Box that can edit the name basket
     * @param view
     */
    public void onClickEditNameBasket(View view) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        builder.setTitle("تعویض نام سبد خرید");

        final EditText editTextBasketName = new EditText(mContext);
        editTextBasketName.setText(activeBasketName.getText().toString());
        editTextBasketName.setInputType(InputType.TYPE_CLASS_TEXT);
        editTextBasketName.setGravity(Gravity.RIGHT);
        builder.setView(editTextBasketName);


        builder.setPositiveButton("ذخیره", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditNameBasket(basketServerId, editTextBasketName.getText().toString());
            }
        });

        builder.setNegativeButton("انصراف", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Icode.ShowShortToast(mContext, "انصراف کلیک شد");
            }
        });

        builder.show();

    }

    /**
     * update the name basket first Server
     * then DataBase
     * then change TextView
     *
     * @param basketServerId
     * @param nameBasket
     */
    public void EditNameBasket(final int basketServerId, final String nameBasket) {

        String Url = this.baseUrl + "update/basket";

        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("basket_id", basketServerId);
        params.put("name", nameBasket);

        client.post(Url, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Icode.ShowShortToast(mContext, "خطا در به روز رسانی - مجدد تلاش کنید");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                getResponseEditName(responseString, nameBasket, basketServerId);
            }
        });
    }

    /**
     * get Response the name Basket
     * @param responseString
     */
    public void getResponseEditName(String responseString, String nameBasket, int basketServerId) {

        Gson gson = new Gson();
        UpdateBasketModel updateBasketModel = gson.fromJson(responseString, UpdateBasketModel.class);

        if(updateBasketModel.getStatusCode().equals(200)) {

            bdHandler.updateNameBasket(basketServerId, nameBasket);

            activeBasketName.setText(nameBasket);
        }
    }
}
