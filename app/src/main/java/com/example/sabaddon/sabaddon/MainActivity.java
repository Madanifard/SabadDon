package com.example.sabaddon.sabaddon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;

import com.orhanobut.hawk.Hawk;

public class MainActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Hawk.init(mContext).build();

        setContentView(R.layout.activity_main);


    }

    /**
     * on lick function for btn_add_basket
     * @param v
     */
    public void redirect_new_basket(View v) {

        Intent intent = new Intent(mContext, AddBasketActivity.class);
        startActivity(intent);
    }

    /**
     * onCLick function for redirect to basket
     * @param v
     */
    public void redirect_active_list_baskets(View v) {

        Intent intent = new Intent(mContext, ActiveBasketActivity.class);
        startActivity(intent);
    }

    /**
     * this function is temp
     * @param view
     */
    public void delete_registry(View view) {
     //   Hawk.delete("register");
       // Icode.ShowShortToast(mContext, "Delete!");
    }
}
