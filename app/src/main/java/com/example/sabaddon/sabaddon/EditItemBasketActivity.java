package com.example.sabaddon.sabaddon;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.alirezaafkar.sundatepicker.DatePicker;
import com.alirezaafkar.sundatepicker.components.DateItem;
import com.alirezaafkar.sundatepicker.interfaces.DateSetListener;
import com.example.sabaddon.sabaddon.DBHandler.BasketDBHandler;
import com.example.sabaddon.sabaddon.Models.DeleteBasketItemModel.DeleteBasketItemModel;
import com.example.sabaddon.sabaddon.Models.GetUnitModel.GetUnitModel;
import com.example.sabaddon.sabaddon.Models.GetUnitModel.Unit;
import com.example.sabaddon.sabaddon.Models.UpdateBasketItemModel.UpdateBasketItemModel;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

public class EditItemBasketActivity extends BaseActivity {

    int productItemId, serverBasketId;

    BasketDBHandler basketDBHandler;

    TextView nameProductEdit;
    Spinner drpUnitEdit;
    EditText countProductEdit, priceItemEdit, dateItemEdit;

    String[] dataItem;

    Button btnSelectDatePiker;

    private Date mDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item_basket);

        productItemId = Integer.parseInt(getIntent().getExtras().get("product_item_id").toString());
        serverBasketId = Integer.parseInt(getIntent().getExtras().get("server_basket_id").toString());

        getUnits(); //get unit from Server

        basketDBHandler =  new BasketDBHandler(mContext, "sabaddon.db", null, 1);
        dataItem = basketDBHandler.getBasketItem(productItemId);

        bindAndSetData(dataItem);

    }

    /**
     * get data from DataBase
     * and Set to widget for Update
     */
    public void bindAndSetData(String[] dataItem) {

        nameProductEdit = (TextView) findViewById(R.id.name_product_edit);
        nameProductEdit.setText(dataItem[0]);

        drpUnitEdit = (Spinner) findViewById(R.id.drp_unit_edit);

        countProductEdit = (EditText) findViewById(R.id.count_product_edit);
        countProductEdit.setText(dataItem[2]);

        dateItemEdit = (EditText) findViewById(R.id.date_item_edit);
        dateItemEdit.setText(dataItem[3]);

        priceItemEdit = (EditText) findViewById(R.id.price_item_edit);
        priceItemEdit.setText(dataItem[4]);
    }

    /**
     * call wbe service for get Unit
     */
    public void getUnits() {

        String getUnitUrl = baseUrl + "get/units";

        AsyncHttpClient client = new AsyncHttpClient();

        client.get(getUnitUrl, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Icode.ShowShortToast(mContext, throwable.toString());
                Log.d("Debug", " debug :  getProduct : ", throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseUnitGson(responseString);

                drpUnitEdit.setSelection(Integer.parseInt(dataItem[1])); // set the Default value
            }
        });
    }

    /**
     * parse and get Units
     * @param response_string
     */
    public void parseUnitGson(String response_string) {

        Gson gson = new Gson();

        GetUnitModel unitModel = gson.fromJson(response_string, GetUnitModel.class);

        if(unitModel.getStatusCode().equals(200)) {

            ArrayList<String> unitArray = new ArrayList<String>();
            unitArray.add(0, "انتخاب واحد محصول");

            for (Unit unit : unitModel.getUnits()) {
                basketDBHandler.manageUnits(unit.getId(), unit.getName());

                unitArray.add(unit.getId(), unit.getName());
            }

            ArrayAdapter unitAdapter = new ArrayAdapter(this, R.layout.spinner_row ,unitArray);
            drpUnitEdit.setAdapter(unitAdapter);

        } else {
            Icode.ShowShortToast(mContext, "Error in get Units");
        }

    }

    /**
     * for delete the item
     * @param view
     */
    public void onClickDelete(View view) {

        ws_delete_item_product(serverBasketId, productItemId);
    }

    /**
     * delete basket item in server
     * @param server_basket_id
     * @param product_id
     */
    public void ws_delete_item_product(int server_basket_id, int product_id) {

        String url = baseUrl + "delete/basket/item";

        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("basket_id", server_basket_id);
        params.put("product_id", product_id);

        client.post(url, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Icode.ShowShortToast(mContext, "خطا در حذف - مجدد تلاش کنید");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseResponseStringGson(responseString);
            }
        });
    }

    /**
     * parse the response the Delete Item and Delete from device
     * @param responseString
     */
    public void parseResponseStringGson(String responseString) {

        Gson gson = new Gson();
        DeleteBasketItemModel deleteBasketItemModel = gson.fromJson(responseString, DeleteBasketItemModel.class);
        if(deleteBasketItemModel.getStatusCode().equals(200)){

            basketDBHandler.deleteBasketItems(serverBasketId, productItemId);

            Intent intent = new Intent(mContext, DetailsBasketActivity.class);
            intent.putExtra("serverBasketId", serverBasketId);
            startActivity(intent);
        }
    }

    /**
     * for save the Data
     * @param view
     */
    public void onClickSave(View view) {

        int unitServerId = (int) drpUnitEdit.getSelectedItemId();
        int count = Integer.parseInt(countProductEdit.getText().toString());
        int price = Integer.parseInt(priceItemEdit.getText().toString());
        String date_buy = dateItemEdit.getText().toString();

        //save to DataBase
        basketDBHandler.updateItemBasket(productItemId, unitServerId, count, date_buy, price);

        //call webService
        ws_update_basket_item(productItemId, unitServerId, count, date_buy, price);
    }


    /**
     * call webService for update basket item in Server
     * @param productItemId
     * @param unitServerId
     * @param count
     * @param date_buy
     * @param price
     */
    public void ws_update_basket_item(int productItemId, int unitServerId, int count,String date_buy, int price) {

        String url = baseUrl + "update/basket/item";

        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("product_id", productItemId);
        params.put("count", count);
        params.put("price", price);
        params.put("date_buy", date_buy);

        client.post(url, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Icode.ShowShortToast(mContext, "خطا در ارتباط با سرور رخ داده است - مجدد تلاش کنید");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                getResponseGson(responseString);
            }
        });

    }

    /**
     * get response from server and redirect to  DetailsBasketActivity
     * @param responseString
     */
    public void getResponseGson(String responseString) {

        Gson gson = new Gson();

        UpdateBasketItemModel basketItemModel = gson.fromJson(responseString, UpdateBasketItemModel.class);

        if(basketItemModel.getStatusCode().equals(200)) {

            Intent intent = new Intent(mContext, DetailsBasketActivity.class);
            intent.putExtra("serverBasketId", serverBasketId);
            startActivity(intent);

        } else {
            Icode.ShowShortToast(mContext, "خطا در عملیات");
        }
    }

    /**
     * when click cancel button
     * @param view
     */
    public void onClickBtnCancel(View view) {

        Intent intent = new Intent(mContext, DetailsBasketActivity.class);
        intent.putExtra("serverBasketId", serverBasketId);
        startActivity(intent);
    }

    /**
     * for Show the Date Picker
     * @param view
     */
    public void onClickBtnDatePicker(View view) {
        setDatePicker();
    }

    /**
     * for DatePicker
     */
    public void setDatePicker(){

        DatePicker.Builder builder = new DatePicker
                .Builder()
                .theme(R.style.DialogTheme)
                .future(true);
        mDate = new Date();
        builder.date(mDate.getDay(), mDate.getMonth(), mDate.getYear());
        builder.build(new DateSetListener() {
            @Override
            public void onDateSet(int id, @Nullable Calendar calendar, int day, int month, int year) {
                mDate.setDate(day, month, year);

                //textView
                dateItemEdit.setText(mDate.getDate());

            }
        }).show(getSupportFragmentManager(), "");
    }

    class Date extends DateItem {
        String getDate() {
            Calendar calendar = getCalendar();
            return String.format(Locale.US,
                    "%d/%d/%d",
                    getYear(), getMonth(), getDay(),
                    calendar.get(Calendar.YEAR),
                    +calendar.get(Calendar.MONTH) + 1,
                    +calendar.get(Calendar.DAY_OF_MONTH));
        }
    }
}


