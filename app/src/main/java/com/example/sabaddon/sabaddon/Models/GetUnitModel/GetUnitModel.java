
package com.example.sabaddon.sabaddon.Models.GetUnitModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetUnitModel {

    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("units")
    @Expose
    private List<Unit> units = null;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Unit> getUnits() {
        return units;
    }

    public void setUnits(List<Unit> units) {
        this.units = units;
    }

}
