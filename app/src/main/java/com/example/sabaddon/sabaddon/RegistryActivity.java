package com.example.sabaddon.sabaddon;

import android.content.Intent;
import android.icu.text.IDNA;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.example.sabaddon.sabaddon.Models.InsertMemberModel.InsertMemberModel;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.orhanobut.hawk.Hawk;

import java.io.IOException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.HttpContext;

public class RegistryActivity extends BaseActivity {

    EditText tel;

    private TelephonyManager mTelephonyManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registery);

        Hawk.init(mContext).build();

//        ws_getToken();

        widget();
    }

    /**
     * bind widget
     */
    public void widget() {
        tel = (EditText)findViewById(R.id.edit_tel);
    }

    /**
     * onclick for registerButton
     * @param view
     */
    public void registerClick(View view) {

        String mobile = tel.getText().toString();

        if (mobile.substring(0,2).equals("09")) {

//            mTelephonyManager = (TelephonyManager) getSystemService(mContext.TELEPHONY_SERVICE);
//            String imei = mTelephonyManager.getDeviceId();
            String imei = "123456789";
//            Long token = this.token;
            String token = "token";
            String nameMobile = android.os.Build.MODEL;

            ws_insert_member(imei, token, mobile, nameMobile);


        } else {

            Icode.ShowShortToast(mContext, "لطفا شماره موبایل معتبر وارد کنید");
        }

    }

    /**
     * cal insert/member webservice
     * @param imei
     * @param token
     * @param mobile
     * @param nameMobile
     */
    public void ws_insert_member(String imei, String token, String mobile, String nameMobile) {

        String urlInsertMember = this.baseUrl + "insert/member";

        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("_token", token);
        params.put("imei", imei);
        params.put("mobile", mobile);
        params.put("name_device", nameMobile);

        client.post(urlInsertMember, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                Icode.ShowShortToast(mContext, throwable.toString());

                Log.d("Debug", "debug : insert/member : " + baseUrl, throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.d("Debug", "debug : responseString : " + responseString);
                parseInsertMember(responseString);
            }
        });
    }

    /**
     * parse insert/member webservice
     * @param response_string
     */
    public void parseInsertMember(String response_string) {

        Gson gson = new Gson();
        InsertMemberModel member = gson.fromJson(response_string, InsertMemberModel.class);

        if(member.getStatusCode() == 200) {

            Hawk.put("memberId", member.getMemberId());
            Hawk.put("register", true);

            Intent intent = new Intent(mContext, MainActivity.class);
            startActivity(intent);
        }
    }
}
