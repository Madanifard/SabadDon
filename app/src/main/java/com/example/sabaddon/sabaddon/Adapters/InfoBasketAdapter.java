package com.example.sabaddon.sabaddon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.sabaddon.sabaddon.R;

import java.util.zip.Inflater;

/**
 * Created by Hello_world on 11/25/2017.
 */

public class InfoBasketAdapter extends BaseAdapter {

    Context mContext;
    String items[][];

    public InfoBasketAdapter(Context mContext, String items[][]) {

        this.mContext = mContext;
        this.items = items;
    }

    @Override
    public int getCount() {

        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View viewInflate = LayoutInflater.from(mContext).inflate(R.layout.info_item_basket, parent, false);

        TextView name_item = (TextView )viewInflate.findViewById(R.id.name_item);
        name_item.setText(items[position][1]);

        TextView count_item = (TextView)viewInflate.findViewById(R.id.count_item);
        count_item.setText(items[position][2]);

        TextView price_item = (TextView) viewInflate.findViewById(R.id.price_item);
        price_item.setText(items[position][4]);

        TextView date_item = (TextView) viewInflate.findViewById(R.id.date_item);
        date_item.setText(items[position][3]);

        TextView unit_item = (TextView) viewInflate.findViewById(R.id.unit_item);
        unit_item.setText(items[position][5]);

        TextView product_id = (TextView) viewInflate.findViewById(R.id.product_item_id);
        product_id.setText(items[position][6]);

        TextView server_basket_id = (TextView) viewInflate.findViewById(R.id.server_basket_id);
        server_basket_id.setText(items[position][0]);

        return viewInflate;
    }
}
