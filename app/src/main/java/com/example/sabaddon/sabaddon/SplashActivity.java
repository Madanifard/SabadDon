package com.example.sabaddon.sabaddon;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.orhanobut.hawk.Hawk;

public class SplashActivity extends BaseActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        Hawk.init(mContext).build();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(Hawk.contains("register")) {

                    //got to MainActivity
                    Intent intent = new Intent(mContext, MainActivity.class);
                    startActivity(intent);

                } else {

                    //redirect to Register Activity
                    Intent intent = new Intent(mContext, RegistryActivity.class);
                    startActivity(intent);
                }

                finish(); // show One and not return in this page
            }
        }, 3000);


    }
}
