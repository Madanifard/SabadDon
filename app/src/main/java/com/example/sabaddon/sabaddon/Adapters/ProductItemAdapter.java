package com.example.sabaddon.sabaddon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.example.sabaddon.sabaddon.R;

/**
 * Created by Hello_world on 11/5/2017.
 */

public class ProductItemAdapter extends BaseAdapter {

    Context mContext;
    String nameProduct[];
    String countProduct[];
    String unitProduct[];

    public ProductItemAdapter(Context mContext, String[] nameProduct, String[] countProduct, String[] unitProduct) {

        this.mContext = mContext;
        this.nameProduct = nameProduct;
        this.countProduct = countProduct;
        this.unitProduct = unitProduct;

    }

    @Override
    public int getCount() {
        return nameProduct.length;
    }

    @Override
    public Object getItem(int position) {
        return nameProduct[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View rowView = LayoutInflater.from(mContext).inflate(R.layout.lis_product_item, parent, false);

        TextView name_product = (TextView) rowView.findViewById(R.id.listNameProduct);
        TextView count_product = (TextView) rowView.findViewById(R.id.listCountProduct);
        TextView unit_product = (TextView) rowView.findViewById(R.id.listUnitProduct);

        name_product.setText(nameProduct[position]);
        count_product.setText(countProduct[position]);
        unit_product.setText(unitProduct[position]);

        return rowView;
    }
}
